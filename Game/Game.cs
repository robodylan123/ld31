﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
//Import SFML.NET
using SFML.Graphics;
using SFML.Window;
using SFML.Audio;
//Import Farseer Physics
using FarseerPhysics.Dynamics;
//Import XNA Framework
using Microsoft.Xna.Framework;
namespace LD30
{
    class Game
    { 
        //Graphics
        public static Font font = new Font("content/Font.ttf");
        public static Text score1;
        public static Text score2;
        public static Text timeLeft;
        public static Music music;
        //Physics Variables
        public static World world;
        public static TimeSpan elapsed;
        public static Stopwatch stopwatch = new Stopwatch(); 
        //Window Variables
        public static int mouseX;
        public static int mouseY;
        public static RenderWindow window;
        public static int width = (64 * 16) + 1;
        public static int height = (64 * 10) + 1;
        public static String title;
        //Game Variables
        public static int fps = 0;
        public static int Frames;
        public static Player player1;
        public static Player player2;
        public static int BG = 0;
        public static bool Growing = false;
        public static int currentLevel = 1;
        //Scoring
        public static int P1points = 0;
        public static int P2points = 0;
        public static int time = 30;
        public static void Init()
        {
            music = new Music("content/audio/music.wav");
            stopwatch.Start();
            world = new World(new Microsoft.Xna.Framework.Vector2(0.0f, 9.82f));
            Level.Load(currentLevel);            
            window = new RenderWindow(new VideoMode((uint)width, (uint)height), title);
            player1 = new Player(64 * 2, 64 * 2);
            player2 = new Player(width-(64*3),64*2);
            player1.sprite.Color = Color.Blue;
            player2.sprite.Color = Color.Red;
            Level.entities.Add(player1);
            Level.entities.Add(player2);
            window.KeyPressed += Input;
            music.Loop = true;
            music.Play();
            window.Closed += new EventHandler(OnClose);
            //window.KeyReleased += KeyUp;            
            while (window.IsOpen())
            {
                if((time * currentLevel) - (elapsed.Seconds + (elapsed.Minutes * 60)) == 1)
                {
                    currentLevel++;
                    Level.entities.Clear();
                    Level.blocks.Clear();
                    world.Clear();
                    player1 = new Player(64 * 2, 64 * 2);
                    player2 = new Player(width - (64 * 3), 64 * 2);
                    player1.sprite.Color = Color.Blue;
                    player2.sprite.Color = Color.Red;
                    Level.entities.Add(player1);
                    Level.entities.Add(player2);
                    Level.Load(currentLevel);
                }
                elapsed = stopwatch.Elapsed;         
                window.DispatchEvents();
                float ts = (float)elapsed.TotalSeconds;
                fps = (int)(Frames / ts) + 1;
                world.Step(1f / fps);
                Render();
                window.Display();
                Frames++;
            }
            window.Dispose();
        }
        public static void Input(Object sender, KeyEventArgs e)
        {
            //Player 1
            player1.body.Friction = 500f;
            if (e.Code == Keyboard.Key.A)
            {
                player1.body.LinearVelocity = new Vector2(-20, player1.body.LinearVelocity.Y);
            }
            if (e.Code == Keyboard.Key.D)
            {
                player1.body.LinearVelocity = new Vector2(20, player1.body.LinearVelocity.Y);
            }
            if (e.Code == Keyboard.Key.W && player1.isOnGround)
            {
                player1.body.LinearVelocity = new Vector2(player1.body.LinearVelocity.X, -30);
            }
            //Player 2
            player2.body.Friction = 500f;
            if (e.Code == Keyboard.Key.Left)
            {
                player2.body.LinearVelocity = new Vector2(-20, player2.body.LinearVelocity.Y);
            }
            if (e.Code == Keyboard.Key.Right)
            {
                player2.body.LinearVelocity = new Vector2(20, player2.body.LinearVelocity.Y);
            }
            if (e.Code == Keyboard.Key.Up && player2.isOnGround)
            {
                player2.body.LinearVelocity = new Vector2(player2.body.LinearVelocity.X, -30);
            } 
            //Check for reset
            if (e.Code == Keyboard.Key.R)
            {
                System.Diagnostics.Process.Start("LD31.exe");
                window.Dispose();
            }
        }
        public static void Render()
        {
            if (Growing)
            {
                BG++;
            }
            else
            {
                BG--;
            }
            if (BG >= 255 * 4)
            {
                Growing = false;
            }
            if(BG <= 0)
            {
                Growing = true;
            }
            window.Clear(new Color((byte)(BG / 4),(byte)(BG / 4),(byte)(BG / 4)));
            
            foreach (Block b in Level.blocks)
            {
                b.sprite.Position = new Vector2f((b.body.Position.X) * 10.0f, (b.body.Position.Y) * 10.0f);
                window.Draw(b.sprite);
            }
            foreach (Entity e in Level.entities)
            {
                e.sprite.Rotation = e.body.Rotation * 160; 
                e.sprite.Position = new Vector2f((e.body.Position.X * 10) + 34,(e.body.Position.Y * 10) + 34);
                window.Draw(e.sprite);
                if(e.sprite.Position.Y > height && e.hasScored == false && !Level.isLastLevel)
                {
                    e.hasScored = true;
                    if (e.sprite.Position.X > 0 && e.sprite.Position.X < width / 2)
                    {
                        P2points++;
                    }
                    if (e.sprite.Position.X > width / 2 && e.sprite.Position.X < width)
                    {
                        P1points++;
                    }
                }
            }   
            score1 = new Text(P1points.ToString(), font);
            score1.Position = new Vector2f(256, 256);
            score2 = new Text(P2points.ToString(), font);
            score2.Position = new Vector2f(width - (256 + 64), 256);
            timeLeft = new Text(((time * currentLevel) - (elapsed.Seconds + (elapsed.Minutes * 60))).ToString(), font);
            timeLeft.Position = new Vector2f(width / 2, height / 2); 
            if (BG > (255 * 2))
            {
                score1.Color = Color.Black;
                score2.Color = Color.Black;
                timeLeft.Color = Color.Black;
            }
            else
            {
                timeLeft.Color = Color.White;
                score1.Color = Color.White;
                score2.Color = Color.White;
            }
            score1.CharacterSize = 100;
            score2.CharacterSize = 100;
            window.Draw(score1);
            window.Draw(score2);
            if(!Level.isLastLevel) window.Draw(timeLeft);
            else
            {   
                timeLeft = new Text("Final Scores", font);
                timeLeft.Position = new Vector2f((width / 2) - 120, height / 2); 
                timeLeft.CharacterSize = 50;
                timeLeft.Color = Color.Red;
                window.Draw(timeLeft);
                timeLeft = new Text("Press \"R\" To Reset", font);
                timeLeft.Position = new Vector2f((width / 2) - 120, (height / 2) + 60);
                timeLeft.CharacterSize = 30;
                timeLeft.Color = Color.Red;
                window.Draw(timeLeft);
            } 
        }

        public static void OnClose(Object sender, EventArgs e)
        {
            window.Dispose();
        }

    }
}
