﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//Import SFML
using SFML.Graphics;
//Import File Stream
using System.IO;
namespace LD30
{
    class Level
    {
        public static bool isLastLevel = false;
        public static List<Block> blocks = new List<Block>();
        public static List<Entity> entities = new List<Entity>();
		public static void Load(int i)
		{
            if(File.Exists("content/images/Level/levelBlock" + i +".png"))
            {
            Image image = new Image("content/images/Level/levelBlock" + i +".png");            
			    for (uint y = 0; y < image.Size.Y; y++)
			    {
                    for (uint x = 0; x < image.Size.X; x++)
                    {
                        if (image.GetPixel(x, y).B == 255)
                        {
                            blocks.Add(new Block((int)x * Block.blockSize,(int)y * Block.blockSize, getBlockID(image.GetPixel(x, y))));
                        }
                        else if (image.GetPixel(x, y).R == 255)
                        {
                            entities.Add(new Entity((int)x * Block.blockSize,(int)y * Block.blockSize, "content/images/Chars/player/"));
                        }
                        else if (image.GetPixel(x,y).G == 255)
                        {
                            blocks.Add(new Goal((int)x * Block.blockSize,(int) y * Block.blockSize));
                        }
                    }
                    					
			    }
            }
            else
            {
                isLastLevel = true;
            }
            //image = new Image("content/images/Level/levelEntity.png");
		}
        private static int getBlockID(Color color)
        {
            return 1;
        }

        private static int getEntityID(Color color)
        {
            return 1;
        }
    }
}
