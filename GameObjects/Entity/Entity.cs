﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//Import SFML
using SFML.Graphics;
using SFML.Window;
//Import
using FarseerPhysics.Collision;
using FarseerPhysics.Factories;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Common;
using FarseerPhysics.Controllers;
using FarseerPhysics;
using Microsoft.Xna.Framework; 

namespace LD30
{
    public class Entity
    {
        //Graphics
        public Sprite sprite;
        public Texture texture;
        //Physics
        public Body body;
        public bool isOnGround; 
        //Animation
        public int Frame = 1;
        public string image;
        public bool hasScored = false;
        public Entity(int x, int y, string image)
        {
            isOnGround = true;
            this.image = image;
            this.texture = new Texture(image + 2 + ".png");
            this.sprite = new Sprite(texture);
            this.sprite.Origin = new Vector2f(32, 32);
            CreatePhysicsBody();
            SetupPhysicsBody(x, y);
            Random r = new Random(DateTime.Now.Millisecond);
            sprite.Color = new Color(0,0,0);
        }
        public void Animate(int i){
            if(Frame <= 1){
                Frame++;
            }
            else
            {
                Frame = 1;
            }
            if (i == 0 && Game.Frames % 4 == 0) 
            {
                sprite.Texture = new Texture(image + Frame.ToString() + ".png");
            }
            if (i == 1 && Game.Frames % 4 == 0)
            {
                sprite.Texture = new Texture(image + Frame.ToString() + "R.png");                 
            }
        }

        public bool onCollision(Fixture fixtureA, Fixture fixtureB, FarseerPhysics.Dynamics.Contacts.Contact contact)
        {
            if (fixtureB.CollisionCategories == Category.Cat5)
            {
                isOnGround = true;
            }
            return true;
        }

        public void onSeparation(Fixture fixtureA, Fixture fixtureB)
        {
            isOnGround = false;
        }

        public void CreatePhysicsBody()
        {
            //this.body = BodyFactory.CreateRectangle(Game.world, 6.4f, 6.4f, 3f);
            this.body = BodyFactory.CreateCircle(Game.world, 31.5f * .1f, 3f);
            this.body.BodyType = BodyType.Dynamic;
            //this.body.FixedRotation = true; 
        }

        public void SetupPhysicsBody(int x, int y)
        {
            //this.body.LocalCenter = new Vector2(3.2f, 3.2f);
            body.Position = new Vector2(x * .1f, y * .1f);
            body.CollisionCategories = Category.Cat5;
            body.SleepingAllowed = false;
            body.OnCollision += onCollision;
            body.OnSeparation += onSeparation;
            body.Restitution = 0.0f;
        }  
        
    }
}
