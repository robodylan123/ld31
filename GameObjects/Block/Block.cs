﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//Import SFML
using SFML.Graphics;
//Import farseer physics
using FarseerPhysics.Collision;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Joints;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using FarseerPhysics;

namespace LD30
{
    class Block
    {

        //Physics
        public Body body;
        public static int blockSize = 64; 
        //Graphics
        public int id = 0;
        public Sprite sprite;
        public Block(int x, int y, int id)
        {
            //--
            sprite = new Sprite(new Texture(new Image("content/images/Blocks/dirtMid.png"), new IntRect(0, 0, blockSize, blockSize)));
            this.id = id;
            this.body = BodyFactory.CreateRectangle(Game.world, (blockSize) * 0.1f, (blockSize) * 0.1f, .1f);            
            this.body.FixedRotation = true;
            this.body.Awake = true;
            this.body.BodyType = BodyType.Static;
            if (id == 2) this.body.BodyType = BodyType.Static;
            body.Position = new Vector2((x + 1) * .1f, (y + 1) * .1f);
            body.CollisionCategories = Category.Cat5;
            if (id == 2) body.CollisionCategories = Category.Cat4; 
            body.SleepingAllowed = true;            
        }
        public static string getTexture(int id)
        {
            switch (id)
            {
                case 1: return "content/images/Blocks/dirtMid.png";
                case 2: return "content/images/Blocks/dirtCenter.png";
                default: return "content/images/Blocks/error.png";
            }
        }

        public float getX()
        {
            return body.Position.X;
        }

        public float getY()
        {
            return body.Position.Y;
        } 
    }
}
